
io = io.connect();

'Use Strict';

angular.module('bebotApp', [])

.controller("AppCtrl", AppCtrl)
.service("appServ",appServ);

function AppCtrl($scope, $log, $timeout) {
    var appCtrl = this;

    appCtrl.sendMessage = sendMessage;

    appCtrl.messages = [];
    
    // $scope.$on('$viewContentLoaded', function(event) {
    // });

    // Emit ready event.
    io.emit('ready');

    // Listen for the init event.
    io.on('init', function(data) {
        $timeout(function() {
            console.log('init', data);
            appCtrl.messages.push(data);
        })
    });

    // Listen for the got-message event.
    io.on('got-message', function(data) {
        console.log('data',data);
        $timeout(function() {
            appCtrl.busy = true;
        });

        $timeout(function() {
            appCtrl.messages.push(data);
            appCtrl.busy = false;
        }, _.random(300, 2500));
    });

    function sendMessage() {
        var data = {
            msg: appCtrl.chatBox,
            from: 'user'
        };
        console.log('msg', data);

        appCtrl.messages.push(data);

        // Emit talk event
        io.emit('talk', data);

        appCtrl.chatBox = '';

        return false;
    }
}

function appServ() {

}