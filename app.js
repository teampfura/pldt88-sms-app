var request = require('request');
var EventedLoop = require('eventedloop');
var apiai = require('apiai');
var apiaiapp = apiai("631b4664b638416f8a31b4341b8b77bb");

var loop = new EventedLoop();
var url ='http://203.87.236.231:8080/1/smsmessaging/inbound/registrations/4225379/messages'

loop.every('2s', function () {
	request(url, function (error, responses, body) {
	  if (!error) {
	    var obj = JSON.parse(body);
	    if(obj.inboundSMSMessage[0] != undefined) {
		    console.log(obj.inboundSMSMessage[0].message);
		    console.log(obj.inboundSMSMessage[0].senderAddress);
		    console.log(obj.inboundSMSMessage[0].destinationAddress);

		    var msg = obj.inboundSMSMessage[0].message;
		    var id_number = obj.inboundSMSMessage[0].senderAddress;

		    var requestApi = apiaiapp.textRequest(msg, {
				sessionId: id_number
			});

			requestApi.on('response', function(response) {
				console.log(response);

				var data = {
				    'address' : id_number,
				    'message' : response.result.fulfillment.speech,
			    };
				request({
					    url: 'http://203.87.236.231:8080/1/smsmessaging/outbound/4225379/requests', //URL to hit
					    json: data, //Query string data
					    method: 'POST'
					});

			});

			requestApi.on('error', function(error) {
				console.log(error);
			});

			requestApi.end();
		}
		else {
			console.log("------------------------------ PLDT PLDT PLDT ------------------------------");
		}
	  }
	});
});

loop.start();