# README #

This is Team PFURA's PLDT 88 2016 Hackathon repo for the BeBot chatbot SMS headless app using NodeJS, ExpressJS and API.ai..

### Requirements ###

Make sure you've installed the following:

NodeJS version 4.5

--------------------

After cloning this repo, do:

npm install

--------------------

To run the app locally (for prototype only):

node app.js


We use Smart Integrated Messaging Suite API for sending and receiving SMS.

Send Message to - 09473371329
Received Message from - 4225



different recipient due to signal in cebu (According to Smart Representative, Joshvin Matthew Domingo)